extern crate goblin;

use goblin::{error, Object};

use std::env::args;
use std::io::prelude::*;
use std::fs::File;

fn main() {

    // file is inferred to be a String.
    let file;
    let args: Vec<String> = args().collect();

    if args.len() < 2 {
        panic!();
    } else {
        file = args[1].clone();
    }

    // File::open returns an io::Result<File> instance that must be checked for success/failure.
    //
    // A safe way of doing this would be to use a match block.
    // I our case we assume that the input file will always exist, so
    // we're just going to assume that the open operation was a
    // success and extract the output from the result using Result::unwrap().
    //
    // The unwrap function panics if the result was an error.
    let mut a_out: File = File::open(file).unwrap();

    // This is a prevalant pattern in Rust code where
    // we shadow a previously used version of a variable to give it a new meaning.
    // At first, a_out was a File object.
    // Now, it has become a Vec<u8> holding the contents of the file.
    //
    // The semantic meaning is preserved while the local
    // namespace is left uncluttered.
    //
    // Note that the file hasn't been explicitly closed.
    // All data in Rust operates on RAII principles and
    // their destructors (implemented as a method of the Drop trait)
    // are implicitly called after their last usage.
    let a_out: Vec<u8> = {
        let mut tmp = Vec::new();
        // tmp's inferred type is Vec<u8>.
        //
        // It is observed that tmp is initialised with
        // the Vec type's default constructor.
        // This allows the compiler to assume that tmp is a Vec type.
        //
        // At this stage the template parameter is unknown.
        //
        // The template parameter is inferred from the final value
        // of this expression as well as the type expected by
        // File's read_to_end method used to read all of a_out's
        // contents to memory.
        //
        // File::read_to_end expects a mutable reference to a Vec<u8> value.
        a_out.read_to_end(&mut tmp).unwrap();
        tmp // a_out assumes the value of tmp.
    };
    // At this point, a_out's old File value can be safely dropped,
    // as it is never used again. If we wanted to keep the File around,
    // we could have assigned it to a different variable beforehand.


    // We match on the result of goblin::Object::parse. We assume that it succeeds like before.
    match Object::parse(&a_out).unwrap() {
        Object::Elf(elf) => {
            // The type info here is entirely optional and
            // can be completely inferred from the usage.
            let hdr: &goblin::elf::Header = &elf.header;
            dbg!(*hdr);
        },
        // We're only interested in ELF parsing so we panic if we get a non-ELF file for now.
        _ => panic!()
    }
}
